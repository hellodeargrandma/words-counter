FROM debian:10

RUN apt update && \
    apt install --yes \
        cmake \
        make \
        libgtest-dev \
        build-essential \
        g++

RUN cd /usr/src/googletest && \
    cmake . && \
    cmake --build . --target install

RUN useradd --create-home -r builder

USER builder
WORKDIR /home/builder

COPY src src
COPY test test
COPY data data
COPY CMakeLists.txt .


# Build stage

RUN mkdir build
WORKDIR build

RUN cmake ../ && \
    make

USER root
RUN make install
USER builder

WORKDIR /home/builder
