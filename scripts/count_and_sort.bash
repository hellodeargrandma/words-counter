#!/usr/bin/env bash

if [[ "$1" == "" ]]
then
    echo "Usage: $0 <input_file>"
    exit 1
fi

tr "[:upper:]" "[:lower:]" < $1 | \
    tr --complement --squeeze-repeats "[:alpha:]" "\n" |\
    sort | \
    uniq --count | \
    sort --key 1,1nr --key 2,2

