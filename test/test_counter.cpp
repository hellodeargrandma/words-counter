#include <vector>
#include <string>
#include <sstream>

#include <gtest/gtest.h>

#include "words_counter.hpp"


TEST(CounterTests, RegularInputTest)
{
    std::string input = "The, the\nthings...if\n\n, if\n";
    std::istringstream iss(input);

    WordsCounter words_counter;
    std::vector<std::tuple<int, std::string>> res = {
        {2, "if"},
        {2, "the"},
        {1, "things"},
    };

    EXPECT_EQ(words_counter.count_words(iss), res);
}


TEST(CounterTests, EmptyFile)
{
    std::string input = "";
    std::istringstream iss(input);

    WordsCounter words_counter;
    std::vector<std::tuple<int, std::string>> res = {};

    EXPECT_EQ(words_counter.count_words(iss), res);
}


TEST(CounterTests, NoLettersFile)
{
    std::string input = "\b\b\b\"\'<>><...,,,.,.,!$!#!\n\t\b";
    std::istringstream iss(input);

    WordsCounter words_counter;
    std::vector<std::tuple<int, std::string>> res = {};

    EXPECT_EQ(words_counter.count_words(iss), res);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
