#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <exception>
#include <unordered_map>
#include <vector>

#include "words_counter.hpp"


auto sort_int_str_tuple(
        const std::tuple<int, std::string> &a,
        const std::tuple<int, std::string> &b
) -> bool
{
    if (std::get<0>(a) == std::get<0>(b)) {
        return (std::get<1>(a).compare(std::get<1>(b)) < 0);
    }

    return (std::get<0>(a) > std::get<0>(b));
}


auto WordsCounter::count_words(std::istream &input)
    -> std::vector<std::tuple<int, std::string>>
{
    std::unordered_map<std::string, int> dict = {};

    char c = ' ';
    std::string word = "";
    while (input.get(c)) {
        if (c >= 'a' && c <= 'z') {
            word += c;
            continue;
        }
        else if (c >= 'A' && c <= 'Z') {
            word += c - ('A' - 'a');
            continue;
        }

        if (word[0] == '\0') {
            continue;
        }

        try {
            dict.at(word) += 1;
        }
        catch (std::exception &ex) {
            dict[word] = 1;
        }

        word = "";
    }

    std::vector<std::tuple<int, std::string>> res= {};
    for (const auto &p : dict) {
        res.push_back(std::tuple<int, std::string>(p.second, p.first));
    }

    std::sort(res.begin(), res.end(), sort_int_str_tuple);

    return res;
}
