#ifndef SRC_WORDS_COUNTER_HPP
#define SRC_WORDS_COUNTER_HPP

#include <vector>
#include <string>
#include <sstream>
#include <tuple>

class WordsCounter {
public:
    WordsCounter() = default;

    auto count_words(std::istream &input)
        -> std::vector<std::tuple<int, std::string>>;
};

#endif // SRC_WORDS_COUNTER_HPP
