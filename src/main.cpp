#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

#include <words_counter.hpp>


/**
 * Prints sorted vector of tuples. Waits the first item in the vector to be the
 * biggest one.
 */
auto print_tuples_vector(std::vector<std::tuple<int, std::string>> &v) -> void
{
    if (v.size() == 0) {
        return;
    }

    auto first_column_width = log10(std::get<0>(v[0])) + 2;
    for (auto &item : v) {
        std::cout << std::left << std::setw(first_column_width) << std::get<0>(item)
                  << std::get<1>(item) << std::endl;
    }
}

auto main(int argc, char** argv) -> int
{
    if (argc !=2) {
        std::cout << "Usage: " << argv[0] << " <input_file>" << std::endl;
        return 1;
    }

    std::ifstream istream(argv[1]);
    if ( ! istream.good()) {
        std::cout << "Unable to read given file." << std::endl;
        return 1;
    }

    WordsCounter words_counter;
    auto sorted_counts = words_counter.count_words(istream);

    print_tuples_vector(sorted_counts);

    return 0;
}

