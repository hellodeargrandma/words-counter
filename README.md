# Words counter

A simple util that counts unique words in given text file and prints statistics
on output.

# Bash implementation

Simple Bash implementation is [here](scripts/count_and_sort.bash)

Example of usage:

```
$ ./scripts/count_and_sort.bash data/tests/in.txt
     15 the
      5 come
      5 has
      5 said
      5 time
      5 walrus
      1 many
      1 of
      1 talk
      1 things
      1 to
````

# Building and installing

Building of project is configured via CMake.

## Building on local machine

To build it on you local machine:

1. Clone the project

```bash
git clone https://gitlab.com/hellodeargrandma/words-counter.git words_counter
```
2. Build project as always in separate directory
```bash
cd words_counter
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ ..
make
```
3. Run tests
```bash
./test/words_counter_test
```
4. Install
```bash
make install
```

## Building in container

1. Go to the project root dir:
```bash
cd words_counter
```
2. Build docker
```bash
docker build --tag words-counter:latest .
```
3. Inside docker run words counter
```bash
docker run --interactive --tty words-counter bash
words_counter data/tests/in.txt
```